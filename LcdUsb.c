#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/mutex.h>
#include <linux/semaphore.h>
#include <linux/usb.h>
#include <linux/kref.h>
#include <linux/atomic.h>

// We must declare a License
MODULE_LICENSE("GPL");

#define DEVICE_NAME "lcdusb"
#define DEVICE_VENDOR_ID 0x16c0
#define DEVICE_PRODUCT_ID 0x07c7
#define DEVICE_MINOR_BASE 0
#define DEVICE_MAX_BUFF 32
#define DEVICE_MAX_WRITERS 1

// Declarations
static ssize_t lcd_read (struct file * filp, char * userSpaceBuffer, size_t len, loff_t * fPos);
static ssize_t lcd_write(struct file* filp, const char* userSpaceBuffer, size_t len, loff_t* fPos);
static int lcd_open(struct inode* in, struct file* filp);
static int lcd_release(struct inode* in, struct file* filp);
static loff_t lcd_llseek(struct file *filp, loff_t off, int whence);


static int  lcd_probe(struct usb_interface *interface, const struct usb_device_id *id);
static void lcd_disconnect(struct usb_interface *interface);

// USB 
static struct usb_device_id usb_table [] = { 
	{ USB_DEVICE(DEVICE_VENDOR_ID, DEVICE_PRODUCT_ID) }, 
	{ } // Terminating entry
};

// Used for hotplug
MODULE_DEVICE_TABLE (usb, usb_table);

static struct usb_driver lcd_usb_driver = { 
	// There is no owner field anymore.
	//.owner = THIS_MODULE,
	.name = DEVICE_NAME,
	.id_table = usb_table,
	.probe = lcd_probe,
	.disconnect = lcd_disconnect 
};

static struct file_operations lcd_fops = {
	.owner =	THIS_MODULE,
	.read =		lcd_read,
	.write =	lcd_write,
	.open =		lcd_open,
	.release =	lcd_release,
	.llseek = lcd_llseek
};

/* 
 * usb class driver info in order to get a minor number from the usb core,
 * and to have the device registered with devfs and the driver core
 */
static struct usb_class_driver lcd_uclass = {
	.name = "lcd%d",
	.fops = &lcd_fops,
	.minor_base = DEVICE_MINOR_BASE
};

// LCD Custom Device Structure
struct lcd_device {
	struct kref ref;
	struct usb_device * udev;
	__u8 ctrl_endpoint_addr;
	__le16 ctrl_endpoint_size;
	struct mutex mtx;
	char ctrl_buff[DEVICE_MAX_BUFF];
	atomic_t num_writers;
	u8 io_enabled;
};

#define to_lcd_dev(d) container_of(d, struct lcd_device, ref)

static void lcd_delete(struct kref * ref)
{
	struct lcd_device * dev = to_lcd_dev(ref);
	mutex_destroy(&dev->mtx);
	usb_put_dev(dev->udev);
	kfree(dev);
	printk(KERN_ALERT "LCD USB Module - lcd_delete Called - Deleted device structure instance\n");
}

static int __init lcd_init(void)
{
	// register this driver with the USB subsystem
	int ret = usb_register( &lcd_usb_driver );
	if (ret != 0) {
		printk(KERN_ALERT "LCD USB Module - Could not register driver - Error Number: %d\n", ret);
		return ret;
	}
	
	printk(KERN_ALERT "LCD USB Module - Registered with the USB subsystem\n");

	return ret; //return the last status code
}

static void __exit lcd_exit(void)
{
	// unregister this driver from the USB subsystem
	// Calls discnnect callback function automatically if device is still plugged in
	usb_deregister(&lcd_usb_driver);
	printk(KERN_ALERT "LCD USB Module - Unregistered from the USB subsystem\n");
}

/*
This function (described in the section “probe and disconnect in Detail”) is called by the USB core 
when it thinks it has a struct usb_interface that this driver can handle. 
A pointer to the struct usb_device_id that the USB core used to make this decision is also passed to this function. 
If the USB driver claims the struct usb_interface that is passed to it, it should initialize the device properly and return 0. 
If the driver does not want to claim the device, or an error occurs, it should return a negative error value.
*/

// Called from the USB hub kernel thread; Minimise setup time here as that
// thread handles the addition and removal of many USB devices. Function can sleep.
// lcd_probe and lcd_disconnect are mutually exclusive. No locking required.
// See here: https://www.kernel.org/doc/Documentation/usb/callbacks.txt
static int lcd_probe(struct usb_interface *interface, const struct usb_device_id *id) {

	int retval;
	struct lcd_device * dev;

	printk(KERN_ALERT "LCD USB Module - Probe called\n");

	// kmalloc may sleep
	dev = kmalloc(sizeof(struct lcd_device), GFP_KERNEL);
	if (dev == NULL) {
		printk(KERN_ERR "LCD USB Module - Probe: No Memory!\n");
		return -ENOMEM;
	}

	// Get usb_device from interface and add reference count
	dev->udev = usb_get_dev(interface_to_usbdev(interface));
	dev->ctrl_endpoint_addr = dev->udev->ep0.desc.bEndpointAddress;
	dev->ctrl_endpoint_size = dev->udev->ep0.desc.wMaxPacketSize;
	dev->io_enabled = 1;
	mutex_init(&dev->mtx);
	atomic_set(&dev->num_writers, DEVICE_MAX_WRITERS);
	// Keep track of reference count to lcd_device
	kref_init(&dev->ref);
	// Associate the lcd_device data structure with this usb_interface instance
	usb_set_intfdata(interface, dev);

	// Register device file
	retval = usb_register_dev(interface, &lcd_uclass);
	if (retval) {
		// something prevented us from registering this driver
		printk(KERN_ERR "LCD USB Module - Probe: Cannot register device: %d!\n", retval);
		usb_set_intfdata(interface, NULL);
		// reduce refcount of usb device
		kref_put(&dev->ref, lcd_delete);
		return retval;
	}

	// Accept Device
	printk(KERN_ALERT "LCD USB Module - Probe: Accepting device\n");
	return 0;
}

/*
This function is called by the USB core when the struct usb_interface has been removed 
from the system or when the driver is being unloaded from the USB core.
 */
// usb_set_intfdata and usb_get_intfdata seem to be synchronised; no additional synch is required
// Big lock might have been used for older kernel version. In prototypes, this seems not to be used
// any longer
static void lcd_disconnect(struct usb_interface *interface) {
	struct lcd_device * dev = usb_get_intfdata(interface);

	printk(KERN_ALERT "LCD View USB Module - Disconnect called\n");

	// Unregister device file
	usb_deregister_dev(interface, &lcd_uclass);

	// clear from this interface
	usb_set_intfdata(interface, NULL);

	// Wait for current IO and disable future IO
	mutex_lock(&dev->mtx);
	dev->io_enabled = 0;
	mutex_unlock(&dev->mtx);

	// reduce refcount of lcd device
	kref_put(&dev->ref, lcd_delete);

	printk(KERN_ALERT "LCD View USB Module - Disconnect Ready\n");
}


// File Operations

/*
We initialise the private_data member here. Avoid find_interface call every time in read/write operations
*/
static int lcd_open(struct inode* in, struct file* filp) {

	struct usb_interface * interface;
	struct lcd_device * dev;
	unsigned minor = iminor(in);

	printk(KERN_ALERT "LCD View USB Module - Open Called, Minor: %d\n", minor);

	interface = usb_find_interface(&lcd_usb_driver, minor);
	if (!interface) {
		printk(KERN_ERR "LCD View USB Module - Open: Cannot find interface with minor %d\n", minor);
		return -ENODEV;
	}

	dev = usb_get_intfdata(interface);
	if (dev == NULL) {
		printk(KERN_ERR "LCD View USB Module - Open: Cannot get lcd_device reference from interface %d\n", minor);
		return -ENODEV;
	}

	// Check access
	if (((filp->f_flags & O_ACCMODE) != O_RDONLY) && atomic_dec_return(&dev->num_writers) < 0 ) {
		atomic_inc(&dev->num_writers);
		return -EBUSY;
	}

	// Increment reference count
	kref_get(&dev->ref);

	// Save our object in the file's private structure
	filp->private_data = dev;

	return 0;
}

// release and remove private_data from filp
// no sync with read/write/seek required, these wont be called for 
// the same file instance which is now released
static int lcd_release(struct inode* in, struct file* filp) {

	struct lcd_device * dev = filp->private_data;

	printk(KERN_ALERT "LCD View USB Module - Release Called, Minor: %d\n", iminor(in));

	if (dev == NULL) {
		printk(KERN_ERR "LCD View USB Module - Release: Cannot release..\n");
		return -ENODEV;
	}

	// Increment
	if ((filp->f_flags & O_ACCMODE) != O_RDONLY) {
		atomic_inc(&dev->num_writers);
	}

	// Remove reference
	filp->private_data = NULL;

	// reduce refcount of lcd device
	kref_put(&dev->ref, lcd_delete);

	return 0;
}

/* bmRequestType field in USB setup:
 * d t t r r r r r, where
 * d ..... direction: 0=host->device, 1=device->host
 * t ..... type: 0=standard, 1=class, 2=vendor, 3=reserved
 * r ..... recipient: 0=device, 1=interface, 2=endpoint, 3=other
 * makes 0xC2: d=1, t=2, r=2
 */
static ssize_t lcd_read(struct file* filp, char* userSpaceBuffer, size_t len, loff_t* fPos)
{
	int readLen;
	struct lcd_device * dev;

	// Check for end of file
	if (*fPos < 0 || *fPos >= DEVICE_MAX_BUFF) {
		return 0;
	}

	// Obtain device structure - dev can never be null because read/write can't be called after release but we assert just in case
	dev = filp->private_data;
	if (dev == NULL) {
		printk(KERN_ERR "LCD View USB Module - Read - Device NULL");
		return -ENODEV;
	}

	// Compute correct length
	len = min(len, (size_t)(DEVICE_MAX_BUFF - *fPos));

	// Critical Section
	if (mutex_lock_interruptible(&dev->mtx) != 0) {
		return -EINTR;
	}

	if (dev->io_enabled == 0) {
		printk(KERN_ALERT "LCD View USB Module - Read failed - Device's disconnect function was called\n");
		readLen = -ENODEV;
		goto error;
	}
	readLen = usb_control_msg(
		dev->udev,
		usb_rcvctrlpipe(dev->udev, dev->ctrl_endpoint_addr),
		0,
		USB_DIR_IN | USB_TYPE_VENDOR | USB_RECIP_ENDPOINT, // 0xc2
		0,
		(__u16) *fPos, //index
		dev->ctrl_buff,
		len,
		0
	);
	if(readLen < 0 || (copy_to_user(userSpaceBuffer, dev->ctrl_buff, readLen) != 0)) {
		// Error
		goto error;
	}

	mutex_unlock(&dev->mtx);

	printk(KERN_ALERT "LCD View USB Module - Read Control Msg Sent: readLen: %d, fPos: %lld\n", readLen, *fPos);

	len = (size_t)readLen;

	// Increment File Position
	*fPos += len;

	return len;

	error:
		mutex_unlock(&dev->mtx);
		return readLen < 0 ? readLen : -EFAULT;
}

/* bmRequestType field in USB setup:
 * d t t r r r r r, where
 * d ..... direction: 0=host->device, 1=device->host
 * t ..... type: 0=standard, 1=class, 2=vendor, 3=reserved
 * r ..... recipient: 0=device, 1=interface, 2=endpoint, 3=other
 * makes 0x42: d=0, t=2, r=2
 */
// TODO: Try writing with LEN 0 if possible
static ssize_t lcd_write(struct file* filp, const char* userSpaceBuffer, size_t len, loff_t* fPos)
{
	int writeLen;
	int err;
	struct lcd_device * dev;

	// Check for end of file
	if (*fPos < 0 || *fPos >= DEVICE_MAX_BUFF) {
		return -EFBIG;
	}

	// Obtain device structure - dev can never be null because read/write can't be called after release but we assert just in case
	dev = filp->private_data;
	if (dev == NULL) {
		printk(KERN_ERR "LCD View USB Module - Write - Device NULL");
		return -ENODEV;
	}

	len = min(len, (size_t)(DEVICE_MAX_BUFF - *fPos));

	// Mutual Exclusion Critical Section
	if (mutex_lock_interruptible(&dev->mtx) != 0) {
		return -EINTR;
	}
	
	if (dev->io_enabled == 0) {
		printk(KERN_ALERT "LCD View USB Module - Write failed - Device's disconnect function was called\n");
		err = -ENODEV;
		goto error;
	}

	// copy data from user space to kernel space
	if(copy_from_user(dev->ctrl_buff, userSpaceBuffer, len) != 0) {
		err = -EFAULT;
		goto error;
	}

	writeLen = usb_control_msg(
		dev->udev,
		usb_sndctrlpipe(dev->udev, dev->ctrl_endpoint_addr),
		1,
		USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_ENDPOINT, //0x42
		0,
		(__u16) *fPos, //index
		dev->ctrl_buff,
		len,
		0
	);

	mutex_unlock(&dev->mtx);

	printk(KERN_ALERT "LCD View USB Module - Write Control Msg Sent: writeLen: %d, fPos: %lld, msg: %d\n", writeLen, *fPos, dev->ctrl_buff[0]);

	if(writeLen < 0) {
		return writeLen < 0 ? writeLen : -EIO;
	}

	len = (size_t)writeLen;

	// Increment File Position
	*fPos += len;

	return writeLen;

	error:
		mutex_unlock(&dev->mtx);
		return err;
}

static loff_t lcd_llseek(struct file *filp, loff_t off, int whence)
{
	loff_t newpos;

	switch(whence) {
		case SEEK_SET:
			newpos = off;
			break;

		case SEEK_CUR:
			newpos = filp->f_pos + off;
			break;

		case SEEK_END:
			newpos = DEVICE_MAX_BUFF + off;
			break;

		default: /* can't happen */
			return -EINVAL;
	}

	if (newpos < 0) {
		return -EINVAL;
	}
	filp->f_pos = newpos;
	return newpos;
}

module_init(lcd_init);
module_exit(lcd_exit);
