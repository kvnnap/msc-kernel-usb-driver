This is the README file for the msc-kernel-usb-driver module.

The msc-kernel-usb-driver is a Linux USB module that 
must be compiled and inserted into the kernel. On a 
typical Ubuntu installation, the required kernel 
header files are already installed.

These programs are required prior to compilation:

1) gcc
2) make 
3) Kernel modules directory is present in /lib/modules/$(shell uname -r)/build

To compile the module execute make:

> make

To insert the module, execute: 

> sudo insmod LcdUsb.ko

Done.

When the LCD device is plugged in, the /dev/lcd0 file will be created.
Only the root user can interact with this file. To allow other users to 
read/write this file, execute:

> sudo chmod o+rw /dev/lcd0

This Linux module has been successfully tested on Ubuntu 14.04.
